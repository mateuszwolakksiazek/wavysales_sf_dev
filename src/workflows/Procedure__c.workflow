<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Delete</fullName>
        <description>Delete</description>
        <protected>false</protected>
        <recipients>
            <recipient>marcin.kulakowski@wavy-apps.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lock_Record</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lock Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lock Procedure</fullName>
        <actions>
            <name>Lock_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Procedure__c.Status_w__c</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Procedure__c.Area_w__c</field>
            <operation>equals</operation>
            <value>Area1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
