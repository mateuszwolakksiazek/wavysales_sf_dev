public with sharing class DashboardSnippetController {
    public DashBoardSnippetController(){}

    public string getDashboardHtml(){
        PageReference dbPage = new PageReference('https://wavy-apps-dev-dev-ed.lightning.force.com/01Z1t000000h5lMEAQ');
        Blob pageBlob = dbPage.getContent();
        return pageBlob.toString();
    }
}