/**
 * Created by mateusz.wolak on 05.11.2019.
 */

/***************************************************************************
 * covers clases: FileUploadController, CommonUtility, TH_Material, CustomLookup
 **/
@IsTest
private class FileUploadControllerTest {

    @testSetup
    private static void testSetup() {
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
        );
        insert contentVersionInsert;
    }

    @isTest
    private static void shouldGetTheSameSize() {
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(1, documents.size());

        List<ContentDocument> contentDocuments = FileUploadController.getContentDocumentsInfo(new List<String> { documents[0].Id});
        System.assertEquals(documents.size(), contentDocuments.size());

    }

    @isTest
    private static void shouldDeleteFile() {
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(1, documents.size());

        Boolean ifDeleted = FileUploadController.deleteContentDocuments(new List<String> { documents[0].Id});
        System.assert(ifDeleted);

        documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(0, documents.size());
    }


    @isTest
    private static void shouldCreateMaterialRecord() {
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(1, documents.size());

        FileUploadController.MaterialWrapper materialWrapper = new FileUploadController.MaterialWrapper();
        materialWrapper.Name = 'sample name';
        materialWrapper.Application = null;
        materialWrapper.Author = UserInfo.getUserId();
        materialWrapper.Category = null;
        materialWrapper.Description = 'sample description';
        //materialWrapper.WhatId;
        materialWrapper.ContentDocumentId = documents[0].Id;
        materialWrapper.ContentSize = 4096;
        materialWrapper.FileExtension = 'jpg';
        materialWrapper.FileType = 'JPG';
        materialWrapper.PublicLink = true;
        materialWrapper.KeyMessages = new List<String>();
        materialWrapper.NewKeyMessages = new List<String>{'sample', 'test'};

        FileUploadController.createMaterialRecords(JSON.serialize(new List<FileUploadController.MaterialWrapper> { materialWrapper }));

        List<WavySales_SE_Material__c> materials = [SELECT Id FROM WavySales_SE_Material__c];
        System.assertEquals(1, materials.size());
    }


    @isTest
    private static void shouldFetchSet() {
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        Set<String> contentDocumentIds = CommonUtility.fetchSet(documents, 'Id');
        System.assertEquals(1, contentDocumentIds.size());
    }

    @isTest
    private static void shouldReturnFetchedRecords() {
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        List<CustomLookup.Records> records = CustomLookup.fetchRecords('ContentDocument', 'Title', 'Test', 10);
        System.assertEquals(documents.size(), records.size());
    }

    @isTest
    private static void shouldUpdateMaterial() {
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(1, documents.size());

        FileUploadController.MaterialWrapper materialWrapper = new FileUploadController.MaterialWrapper();
        materialWrapper.Name = 'sample name';
        materialWrapper.Application = null;
        materialWrapper.Author = UserInfo.getUserId();
        materialWrapper.Category = null;
        materialWrapper.Description = 'sample description';
        //materialWrapper.WhatId;
        materialWrapper.ContentDocumentId = documents[0].Id;
        materialWrapper.ContentSize = 4096;
        materialWrapper.FileExtension = 'jpg';
        materialWrapper.FileType = 'JPG';
        materialWrapper.PublicLink = false;
        materialWrapper.KeyMessages = new List<String>();
        materialWrapper.NewKeyMessages = new List<String>{'sample', 'test'};

        FileUploadController.createMaterialRecords(JSON.serialize(new List<FileUploadController.MaterialWrapper> { materialWrapper }));

        List<WavySales_SE_Material__c> materials = [SELECT Id FROM WavySales_SE_Material__c];
        System.assertEquals(1, materials.size());

        WavySales_SE_Material__c material = materials[0];

        //create ContentDocumentLink  record
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = material.Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;

        Test.startTest();

        material.GenerateExternalUrl__c = true;
        update material;

        Test.stopTest();

        material = [SELECT LinkUrl__c FROM WavySales_SE_Material__c WHERE Id =: material.Id];
        System.assertNotEquals(null, material.LinkUrl__c);

        material.GenerateExternalUrl__c = false;
        update material;

    }

}