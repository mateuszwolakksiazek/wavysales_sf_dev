public class CustomImagesExtendedController {
 public blob picture {get; set;}
    public String errorMessage {get; set;}
    
    private final Custom_Images__c image;
    private ApexPages.StandardController stdController;
    
    public CustomImagesExtendedController(ApexPages.StandardController stdController){
        this.image = (Custom_Images__c)stdController.getRecord();
        this.stdController = stdController;
    }
    
    public PageReference save() {
        errorMessage = '';
        try {
            upsert image;
            if (picture!=null){
                Attachment attachment = new Attachment();
                attachment.Body = picture;
                attachment.Name = 'img__'+ image.Id + '.jpg';
                attachment.ParentId = image.Id;
                attachment.ContentType ='application/jpg';
                insert attachment;
                image.Minature_Path__c ='/servlet/servlet.FileDownload?file=' + attachment.Id;
                update image;
                
            }
            return new ApexPages.StandardController(image).view();
            
        } catch(System.Exception ex){
            errorMessage =ex.getMessage();
            return null;
        }
    }

}