/**
 * Created by mateusz.wolak on 01.11.2019.
 */

public with sharing class CommonUtility {

    /***********************************************************************
    *   VARIABLES
    **/

    /********************
    * START - SOBJECT API NAMES
    ********************/

    public static final String SOBJECT_NAME_WAVY_SALES_KEYMESSAGE = 'WavySales_KeyMessage__c';
    public static final String SOBJECT_NAME_WAVY_SALES_KEYMESSAGE_RELATION = 'WavySales_KeyMessageRelation__c';
    public static final String SOBJECT_NAME_WAVY_SALES_SE_MATERIAL = 'WavySales_SE_Material__c';

    public static final String SOBJECT_NAME_ACCOUNT = 'Account';
    public static final String SOBJECT_NAME_CONTACT = 'Contact';
    public static final String SOBJECT_NAME_EVENT = 'Event';
    public static final String SOBJECT_NAME_TASK = 'Task';
    public static final String SOBJECT_NAME_CASE = 'Case';

    /********************
    * END - SOBJECT API NAMES
    ********************/

    /********************
    * START - Record types
    ********************/


    // WavySales_SE_Material__c
    public static final String MATERIAL_TYPE_FILE = 'MaterialFile';
    public static final String MATERIAL_TYPE_LINK = 'MaterialLink';

    // WavySales_KeyMessageRelation__c
    public static final String KEYMESSAGE_RELATION_TYPE_MATERIAL_KEYWORD = 'MaterialKeyword';

    // WavySales_KeyMessage__c
    public static final String KEYMESSAGE_TYPE_MATERIAL_KEYWORD = 'MaterialKeyword';




    /********************
    * END - Record types
    ********************/


    /********************
    * START - Picklist values
    ********************/

    public static final String KEY_MESSAGE_USAGE_SE_MATERIAL = 'SE_MATERIAL';

    public static final String WAVYSALES_SETTING_APP_SE = 'SE';

    public static final String WAVYSALES_SETTING_COMPONENTSOURCE_SF = 'SF';
    public static final String WAVYSALES_SETTING_COMPONENTTYPE_RECORDSLISTPAGE = 'RecordsListPage';
    
    /********************
    * END - Picklist values
    ********************/


    //messages

    public static final String NO_SUCH_RT_FOUND = 'No such record type found!';
    public static final String OBJECTNAME_AND_RT_NEEDS_TO_BE_DELIVERED = 'ObjectName and RecType needs to be delivered!';


    //constants

    public static final String DOCS_MATCHING_SETTING_NAME = 'Docs.Matching.Settings';
    public static final String DOWNLOAD_LINK_URL = '{0}/sfc/servlet.shepherd/version/download/{1}?operationContext=S1';


    /***********************************************************************
    *   EXCEPTIONS
    **/
    public class CommonUtilityException extends Exception{}


    /***********************************************************************
    *   METHODS
    **/
    public static Map<String, Id> mRecordTypeName2Id = new Map<String, Id>();

    public static Id getRecordTypeId(string objectName, string recTypeDevName){
        if(string.isNotBlank(objectName) && string.isNotBlank(recTypeDevName)){
            string key = createKeyForRtMap(objectName,recTypeDevName);
            if(mRecordTypeName2Id == null || mRecordTypeName2Id.size() == 0){
                RecordType[]rts = [Select SobjectType, Name, Id, DeveloperName From RecordType];
                for(RecordType r1 : rts){
                    putRecordTypeId(r1.SobjectType, r1.DeveloperName, r1.Id);
                }
            }
            if(mRecordTypeName2Id.containsKey(key)){
                return mRecordTypeName2Id.get(key);
            } else {
                throw new CommonUtilityException(NO_SUCH_RT_FOUND + ' [OBJECT: ' + objectName +' RECTYPE: ' + recTypeDevName + ']');
            }
        } else {
            throw new CommonUtilityException(OBJECTNAME_AND_RT_NEEDS_TO_BE_DELIVERED);
        }
    }

    public static void putRecordTypeId(string objectName, string recTypeDevName, Id recTypeId){
        if(string.isNotBlank(objectName) && string.isNotBlank(recTypeDevName) && recTypeId != null){
            mRecordTypeName2Id.put(createKeyForRtMap(objectName, recTypeDevName),recTypeId);
        }
    }

    public static string createKeyForRtMap(string objectName, string recTypeDevName){
        return objectName.toUpperCase() + '.' + recTypeDevName.toUpperCase();
    }

    public static Set<String> fetchSet(List<sObject> objects, String fieldName) {
        Set<String> fieldValues = new Set<String>();
        for (sObject obj : objects) {
            String value = String.valueOf(obj.get(fieldName));
            if (String.isNotBlank(value)) {
                fieldValues.add(value);
            }
        }
        return fieldValues;
    }

}