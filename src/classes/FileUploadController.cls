/**
 * Created by mateuszwolak on 2019-10-16.
 */

public with sharing class FileUploadController {

    /**
    * @author: Mateusz Wolak-Książek
    * @description: get content documents field values
    * @param contentDocumentIds
    *
    * @return
    **/
    //TODO: add wraping (model => {'data' : JSON(return info), 'error' : JSON(error info)}
    @AuraEnabled(cacheable=true)
    public static List<ContentDocument> getContentDocumentsInfo(List<String> documentIds) {
        return [SELECT Id, ContentSize, FileExtension, FileType, Title, OwnerId, Owner.Name FROM ContentDocument WHERE Id IN: documentIds];
    }

    @AuraEnabled(cacheable=false)
    public static Boolean deleteContentDocuments(List<String> documentIds) {
        List<ContentDocument> contentDocuments = new List<ContentDocument>();

        for(String docId : documentIds) {
            contentDocuments.add(new ContentDocument(Id = docId));
        }

        //TODO: add error handling
        delete contentDocuments;

        return true;
    }


    @AuraEnabled(cacheable=false)
    public static String createMaterialRecords(String materialRecordsJSON) {
        Map<String, WavySales_SE_Material__c> contentDocumentIdToMaterialRecordsMap = new Map<String, WavySales_SE_Material__c>();
        Map<WavySales_SE_Material__c, List<String>> materialToKeyMessageIds = new Map<WavySales_SE_Material__c, List<String>>();
        Map<WavySales_SE_Material__c, List<String>> materialToNewKeyMessageNames = new Map<WavySales_SE_Material__c, List<String>>();
        List<MaterialWrapper> materialWrappers = parseMaterialRecordsJSON(materialRecordsJSON);
        Set<Id> whatIds = new Set<Id>();

        for(MaterialWrapper material : materialWrappers) {
            WavySales_SE_Material__c wavyMaterial = material.toMaterialRecord();
            contentDocumentIdToMaterialRecordsMap.put(material.ContentDocumentId, wavyMaterial);
            whatIds.add(material.WhatId);
        }

        //TODO: add error handling
        insert contentDocumentIdToMaterialRecordsMap.values();

        for(MaterialWrapper material : materialWrappers) {
            if(material.KeyMessages != null && !material.KeyMessages.isEmpty()) {
                materialToKeyMessageIds.put(contentDocumentIdToMaterialRecordsMap.get(material.ContentDocumentId), material.KeyMessages);
            }
            if(material.NewKeyMessages != null && !material.NewKeyMessages.isEmpty()) {
                materialToNewKeyMessageNames.put(contentDocumentIdToMaterialRecordsMap.get(material.ContentDocumentId), material.NewKeyMessages);
            }
        }

        copyFilesToMaterialRecords(contentDocumentIdToMaterialRecordsMap, whatIds);

        createKeyMessageRelations(materialToKeyMessageIds, materialToNewKeyMessageNames);

        return 'ok';
    }

    /**
    * @author: Connect files to new material records and delete them from the previous record
    *
    * @param Map containing ContentDocumentId as key, and new material record as value
    *
    * @return
    **/
    public static List<ContentDocumentLink> copyFilesToMaterialRecords(Map<String, WavySales_SE_Material__c> contentDocumentIdToMaterialRecordsMap, Set<Id> whatIds) {
        Map<Id, ContentDocumentLink> contentDocumentIdToContentDocumentLinkMap = new Map<Id, ContentDocumentLink>();
        List<ContentDocumentLink> insertLinks = new List<ContentDocumentLink>(),
                deleteLinks = new List<ContentDocumentLink>();

        for(ContentDocumentLink cdl : [SELECT ContentDocumentId, LinkedEntityId, ShareType, Visibility  FROM ContentDocumentLink WHERE ContentDocumentId IN: contentDocumentIdToMaterialRecordsMap.keySet() AND LinkedEntityId IN: whatIds]) {
            contentDocumentIdToContentDocumentLinkMap.put(cdl.ContentDocumentId, cdl);
        }

        for(Id contentDocumentId : contentDocumentIdToContentDocumentLinkMap.keySet()){
            ContentDocumentLink contentDocumentLink = contentDocumentIdToContentDocumentLinkMap.get(contentDocumentId);
            ContentDocumentLink newclnk = contentDocumentLink.clone();

            newclnk.LinkedEntityId =  contentDocumentIdToMaterialRecordsMap.get(contentDocumentId).Id;

            insertLinks.add(newclnk);
            deleteLinks.add(contentDocumentLink);
        }

        if(!insertLinks.isEmpty()) {
            //TODO: add error handling
            insert insertLinks;
        }

        if(!deleteLinks.isEmpty()) {
            //TODO: add error handling
            delete deleteLinks;
        }

        return insertLinks;
    }

    /**
    * @author: Mateusz Wolak-Książek
    * @description: create WavySales_KeyMessageRelation__c junction records
    *
    * @param materialToKeyMessageIds
    **/
    public static void createKeyMessageRelations(Map<WavySales_SE_Material__c, List<String>> materialToKeyMessageIds, Map<WavySales_SE_Material__c, List<String>> materialToNewKeyMessageNames) {
        Map<String, WavySales_KeyMessage__c> newKeyMessages = new Map<String, WavySales_KeyMessage__c>();
        List<WavySales_KeyMessageRelation__c> keyMessageRelations = new List<WavySales_KeyMessageRelation__c>();

        for(WavySales_SE_Material__c material : materialToKeyMessageIds.keySet()) {

            if(materialToKeyMessageIds.get(material) != null && !materialToKeyMessageIds.get(material).isEmpty()) {
                for(String keyMessageId : materialToKeyMessageIds.get(material)) {
                    keyMessageRelations.add(
                        new WavySales_KeyMessageRelation__c(KeyMessageId__c = keyMessageId, MaterialId__c = material.Id, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE_RELATION, CommonUtility.KEYMESSAGE_RELATION_TYPE_MATERIAL_KEYWORD))
                    );
                }
            }
        }

        for(WavySales_SE_Material__c material : materialToNewKeyMessageNames.keySet()) {
            if(materialToNewKeyMessageNames.get(material) != null && !materialToNewKeyMessageNames.get(material).isEmpty()) {
                for(String keyMessageName : materialToNewKeyMessageNames.get(material)) {
                    newKeyMessages.put(keyMessageName, new WavySales_KeyMessage__c(Name__c = keyMessageName, Usage__c = CommonUtility.KEY_MESSAGE_USAGE_SE_MATERIAL, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE, CommonUtility.KEYMESSAGE_TYPE_MATERIAL_KEYWORD)));
                }
            }
        }

        if(!newKeyMessages.isEmpty()) {
            //TODO: add error handling
            insert newKeyMessages.values();

            for(WavySales_SE_Material__c material : materialToNewKeyMessageNames.keySet()) {
                if(materialToNewKeyMessageNames.get(material) != null && !materialToNewKeyMessageNames.get(material).isEmpty()) {
                    for(String keyMessageName : materialToNewKeyMessageNames.get(material)) {
                        keyMessageRelations.add(
                            new WavySales_KeyMessageRelation__c(KeyMessageId__c = newKeyMessages.get(keyMessageName).Id, MaterialId__c = material.Id, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE_RELATION, CommonUtility.KEYMESSAGE_RELATION_TYPE_MATERIAL_KEYWORD))
                        );
                    }
                }
            }
        }

        if(!keyMessageRelations.isEmpty()) {
            //TODO: add error handling
            insert keyMessageRelations;
        }
    }

    /**
    * @author: Mateusz Wolak-Książek
    * @description: create JSONParser and parse records
    *
    * @param materialRecordsJSON
    *
    * @return List containing MaterialWrappers
    **/
    public static List<MaterialWrapper> parseMaterialRecordsJSON(String materialRecordsJSON) {
        JSONParser parser = JSON.createParser(materialRecordsJSON);
        return (List<MaterialWrapper>)parser.readValueAs(List<MaterialWrapper>.class);
    }

    
    //TODO: move to base class ?
    public class MaterialWrapper {
        public String Name;
        public String Application;
        public String Author;
        public String Category;
        public String Description;
        public String WhatId;
        public String ContentDocumentId;
        public Double ContentSize;
        public String FileExtension;
        public String FileType;
        public Boolean PublicLink;
        public List<String> KeyMessages;
        public List<String> NewKeyMessages;

        public WavySales_SE_Material__c toMaterialRecord() {
            return new WavySales_SE_Material__c(
                    Name__c = this.Name,
                    Application__c = this.Application,
                    AuthorId__c = this.Author,
                    Category__c = this.Category,
                    Description__c = this.Description,
                    FileSize__c = this.ContentSize,
                    FileExtension__c = this.FileExtension,
                    FileMimeType__c = this.FileType,
                    GenerateExternalUrl__c = this.PublicLink,
                    RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_SE_MATERIAL, CommonUtility.MATERIAL_TYPE_FILE)
            );
        }

    }

}