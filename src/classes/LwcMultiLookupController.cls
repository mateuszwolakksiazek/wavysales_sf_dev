/**
 * Created by mateusz.wolak on 01.12.2019.
 */

public with sharing class LwcMultiLookupController {
    public LwcMultiLookupController() {

    }
    @AuraEnabled(cacheable=true)
    public static List<SObJectResult> getResults(String ObjectName, String fieldName, String value, List<String> selectedRecId, String additionalWhereClause) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        system.debug(fieldName+'-------------'+ObjectName+'---++----------'+value+'====='+selectedRecId);
        if(selectedRecId == null)
            selectedRecId = new List<String>();

        //uncomment if you want user to input at least one char
        //if(String.isNotEmpty(value)) {
            String query = 'Select Id,'+fieldName+' FROM '+ObjectName+' WHERE '+fieldName+' LIKE \'%' + value.trim() + '%\' AND Id NOT IN: selectedRecId';
            if(String.isNotBlank(additionalWhereClause)) {
                query += ( ' AND ' + additionalWhereClause );
            }
            query += ' LIMIT 15';
            system.debug(query);
            for(sObject so : Database.Query(query)) {
                String fieldvalue = (String)so.get(fieldName);
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id, fieldvalue + '-' + so.Id));
            }
        //}
        return sObjectResultList;
    }

    @AuraEnabled(cacheable=false)
    public static List<SObJectResult> getRelatedRecordResults(String searchRelatedObjectName, String searchRelatedFieldName, String searchRelatedWhereName, String recordId) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        String baseQuery = 'SELECT Id {0} FROM {1} WHERE {2} =: {3}';

        String query = String.format(baseQuery, new List<String> { ', ' + searchRelatedFieldName, searchRelatedObjectName, searchRelatedWhereName, 'recordId' });

        for(sObject so : Database.query(query)) {
            String fieldvalue = '';
            if(searchRelatedFieldName.contains('__r')) {
                fieldvalue = (String)so.getSObject(searchRelatedFieldName.split('\\.')[0]).get(searchRelatedFieldName.split('\\.')[1]);
            } else {
                fieldvalue = (String)so.get(searchRelatedFieldName);
            }

            sObjectResultList.add(new SObjectResult(fieldvalue, so.Id, fieldvalue + '-' + so.Id));
        }

        return sObjectResultList;
    }

    @AuraEnabled(cacheable=false)
    public static void createKeyMessageRelation(String recordId, String keyMessageId) {
        List<WavySales_KeyMessageRelation__c> keyMessageRelations = new List<WavySales_KeyMessageRelation__c>();

        keyMessageRelations.add(new WavySales_KeyMessageRelation__c(KeyMessageId__c = keyMessageId, MaterialId__c = recordId, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE_RELATION, CommonUtility.KEYMESSAGE_RELATION_TYPE_MATERIAL_KEYWORD)));

        insert keyMessageRelations;
    }

    @AuraEnabled(cacheable=false)
    public static void createKeyMessageWithRelation(String recordId, String keyMessageName) {
        List<WavySales_KeyMessage__c> keyMessages = new List<WavySales_KeyMessage__c>();
        List<WavySales_KeyMessageRelation__c> keyMessageRelations = new List<WavySales_KeyMessageRelation__c>();

        keyMessages.add(new WavySales_KeyMessage__c(Name__c = keyMessageName, Usage__c = CommonUtility.KEY_MESSAGE_USAGE_SE_MATERIAL, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE, CommonUtility.KEYMESSAGE_TYPE_MATERIAL_KEYWORD)));

        if(!keyMessages.isEmpty()) {
            insert keyMessages;

            for(WavySales_KeyMessage__c keyMessage : keyMessages) {
                keyMessageRelations.add(new WavySales_KeyMessageRelation__c(KeyMessageId__c = keyMessage.Id, MaterialId__c = recordId, RecordTypeId = CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE_RELATION, CommonUtility.KEYMESSAGE_RELATION_TYPE_MATERIAL_KEYWORD)));
            }

            insert keyMessageRelations;
        }
    }

    @AuraEnabled(cacheable=false)
    public static void deleteKeyMessageRelation(String keyMessageRelationId) {
        if(keyMessageRelationId != null) {
            delete new WavySales_KeyMessageRelation__c(Id = keyMessageRelationId);
        }
    }

    public class SObjectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        @AuraEnabled
        public String recNameWithId;

        public SObJectResult(String recNameTemp, Id recIdTemp, String recNameWithIdTemp) {
            recName = recNameTemp;
            recId = recIdTemp;
            recNameWithId = recNameWithIdTemp;
        }
        public SObJectResult() {

        }
    }
}