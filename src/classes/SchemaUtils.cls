/**
 * Created by mateusz.wolak on 19.12.2019.
 */

public with sharing class SchemaUtils {

    public static final Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

    public class UnsupportedObjectTypeException extends Exception {}
}