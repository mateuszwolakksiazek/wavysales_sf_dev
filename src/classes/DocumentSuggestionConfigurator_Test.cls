/**
 * Created by mateusz.wolak on 20.01.2020.
 */


/***************************************************************************
 * covers clases: DocumentSuggestionConfiguratorController
 **/
@IsTest
private class DocumentSuggestionConfigurator_Test {

    @testSetup
    private static void testSetup() {
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
        );
        insert contentVersionInsert;
    }

    @isTest
    private static void shouldGetAtLeastOneObjectFromSchema() {
        List<DocumentSuggestionConfiguratorController.SchemaObjectWrapper> schemaObjectsWrappers = DocumentSuggestionConfiguratorController.getSchemaObjectsSrv();
        System.assert(schemaObjectsWrappers.size() > 0);
    }

    @isTest
    private static void shouldGetAtLeastOneFieldNameForFirstObject() {
        List<DocumentSuggestionConfiguratorController.SchemaObjectWrapper> schemaObjectsWrappers = DocumentSuggestionConfiguratorController.getSchemaObjectsSrv();
        System.assert(schemaObjectsWrappers.size() > 0);

        List<DocumentSuggestionConfiguratorController.SchemaFieldWrapper> schemaFieldWrappers = DocumentSuggestionConfiguratorController.getSchemaObjectFieldsSrv(schemaObjectsWrappers[0].label);
        System.assert(schemaFieldWrappers.size() > 0);
    }


    @isTest
    private static void shouldNotGetAnyConfiguration() {
        Map<String, WavySales_SettingValue__c> documentSuggestionCurrentSetting = DocumentSuggestionConfiguratorController.getDocumentSuggestionCurrentSettingsSrv();
        System.assertEquals(0, documentSuggestionCurrentSetting.size());
    }


    @isTest
    private static void shouldSaveAngGetOneConfiguration() {
        List<DocumentSuggestionConfiguratorController.SchemaObjectWrapper> schemaObjectsWrappers = DocumentSuggestionConfiguratorController.getSchemaObjectsSrv();
        System.assert(schemaObjectsWrappers.size() > 0);

        List<DocumentSuggestionConfiguratorController.SchemaFieldWrapper> schemaFieldWrappers = DocumentSuggestionConfiguratorController.getSchemaObjectFieldsSrv(schemaObjectsWrappers[0].label);
        System.assert(schemaFieldWrappers.size() > 0);

        Map<String, WavySales_SettingValue__c> documentSuggestionCurrentSetting = DocumentSuggestionConfiguratorController.getDocumentSuggestionCurrentSettingsSrv();
        System.assertEquals(0, documentSuggestionCurrentSetting.size());

        Map<String, List<String>> objectToFieldsMap = new Map<String, List<String>> { schemaObjectsWrappers[0].label => new List<String> { schemaFieldWrappers[0].value } };
        DocumentSuggestionConfiguratorController.saveDocumentSuggestionConfigurationSrv(objectToFieldsMap);

        documentSuggestionCurrentSetting = DocumentSuggestionConfiguratorController.getDocumentSuggestionCurrentSettingsSrv();
        System.assertNotEquals(0, documentSuggestionCurrentSetting.size());
    }


}