public class CancelProcedure {
  public static void updateDeleted(Map<Id,Object> mapOfIds) {
        
        //Converting map of IDs to list of IDs because Maps not supported by future methods
        List<Id> listOfIds =new List<Id>();
		listOfIds.addAll(mapOfIds.keySet());
        
        updateDeletedFuture(listOfIds);
    }
    
    
    @future
    public static void updateDeletedFuture(List<Id> listOfIds) {
        
        //Method receiving list of IDs because Maps not supported by future methods
        //Undeleting the records that were just deleted
        List<Procedure__c> myList = [Select Id,Status_w__c FROM Procedure__c WHERE Id IN :listOfIds ALL ROWS];
        
        
        
        //Flagging the records
        for (Procedure__c proc : myList) {
            if (proc.Status_w__c == 'Published' || proc.Status_w__c == 'Canceled'){
                undelete myList;
          
            proc.Status_w__c = 'Canceled';
        }
       } 
        //Finally updating the records to get them flagged
        update myList;

    }
    
    
}