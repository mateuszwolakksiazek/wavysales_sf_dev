/**
 * Created by mateusz.wolak on 29.11.2019.
 */

public with sharing class TH_Material extends TriggerHandler.DelegateBase {

    /**************************************************************
     * COLLECTION DECLARATIONS
     */
    Map<Boolean, Set<Id>> materialIdsForPublicLinkCreationDeletion;


    /**************************************************************
     * DATA PREPARATION
     */
    public override void prepareBefore() { }

    public override void prepareAfter(){

        materialIdsForPublicLinkCreationDeletion = new Map<Boolean, Set<Id>>();

    }


    public override void afterInsert(Map<Id, sObject> materialsNew){
        List<WavySales_SE_Material__c> matsNew = (List<WavySales_SE_Material__c>)materialsNew.values();
        for(WavySales_SE_Material__c mtN : matsNew){

            //for new records create public link only if field is checked
            if(mtN.GenerateExternalUrl__c) {
                if(materialIdsForPublicLinkCreationDeletion.containsKey(mtN.GenerateExternalUrl__c)) {
                    materialIdsForPublicLinkCreationDeletion.get(mtN.GenerateExternalUrl__c).add(mtN.Id);
                } else {
                    materialIdsForPublicLinkCreationDeletion.put(mtN.GenerateExternalUrl__c, new Set<Id> { mtN.Id });
                }
            }

        }

    }

    public override void afterUpdate(Map<Id, sObject> materialsOld, Map<Id, sObject> materialsNew) {
        Map<Id, WavySales_SE_Material__c> matsOld = (Map<Id, WavySales_SE_Material__c>) materialsOld;
        Map<Id, WavySales_SE_Material__c> matsNew = (Map<Id, WavySales_SE_Material__c>) materialsNew;

        for (Id key : matsNew.keySet()) {
            WavySales_SE_Material__c mtO = matsOld.get(key);
            WavySales_SE_Material__c mtN = matsNew.get(key);

            //material type file section
            if(
                mtO.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_SE_MATERIAL, CommonUtility.MATERIAL_TYPE_FILE) &&
                mtN.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_SE_MATERIAL, CommonUtility.MATERIAL_TYPE_FILE)
            ) {

                //check whetver public link should be created or deleted
                if(mtO.GenerateExternalUrl__c != mtN.GenerateExternalUrl__c) {
                    if(materialIdsForPublicLinkCreationDeletion.containsKey(mtN.GenerateExternalUrl__c)) {
                        materialIdsForPublicLinkCreationDeletion.get(mtN.GenerateExternalUrl__c).add(mtN.Id);
                    } else {
                        materialIdsForPublicLinkCreationDeletion.put(mtN.GenerateExternalUrl__c, new Set<Id> { mtN.Id });
                    }
                }

            }


        }


    }

    public override void finish() {

        //TODO: move to manager / service class
        /**
         * @author: Mateusz Wolak-Książek
         * @description: create or delete public link for selected material records
         */
        if(materialIdsForPublicLinkCreationDeletion != null && materialIdsForPublicLinkCreationDeletion.size() > 0) {

            if(materialIdsForPublicLinkCreationDeletion.containsKey(true) && !materialIdsForPublicLinkCreationDeletion.get(true).isEmpty()) {
               createPublicLinks(materialIdsForPublicLinkCreationDeletion.get(true));
            }

            if(materialIdsForPublicLinkCreationDeletion.containsKey(false) && !materialIdsForPublicLinkCreationDeletion.get(false).isEmpty()) {
                deletePublicLinks(materialIdsForPublicLinkCreationDeletion.get(false));
            }



        }


    }

    /**
     * @author: Mateusz Wolak-Książek
     * @description: create public link for material records (insert ContentDistribution records and update materials LinkUrl__c field)
     * @param materialIds
     */
    @future
    public static void createPublicLinks(Set<Id> materialIds) {
        List<WavySales_SE_Material__c> materialsToUpdate = new List<WavySales_SE_Material__c>();

        List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: materialIds];
        Map<Id, ContentDistribution> linkedEntitydToContentDistributionMap = generatePublicLinks(contentDocumentLinks);
        for(Id linkedEntityId : linkedEntitydToContentDistributionMap.keySet()) {
            materialsToUpdate.add(new WavySales_SE_Material__c(Id = linkedEntityId, LinkUrl__c = linkedEntitydToContentDistributionMap.get(linkedEntityId).DistributionPublicUrl, DownloadLinkUrl__c = linkedEntitydToContentDistributionMap.get(linkedEntityId).ContentDownloadUrl ));
        }

        if(!materialsToUpdate.isEmpty()) {
            update materialsToUpdate;
        }
    }


    /**
      * @author: Mateusz Wolak-Książek
      * @description: get distribution public URL
      *
      * @param contentDocumentIds
      *
      * @return Map where key is LinkedEntityId and value is ContentDistribution record
      **/
    public static Map<Id, ContentDistribution> generatePublicLinks(List<ContentDocumentLink> contentDocumentLinks) {
        Map<Id, ContentDistribution> linkedEntitydToContentDistributionMap = new Map<Id, ContentDistribution>();
        List<ContentDistribution> contentDistributions = new List<ContentDistribution>();
        Set<String> contentDocumentIds = CommonUtility.fetchSet(contentDocumentLinks, 'ContentDocumentId');
        List<ContentVersion> contentVersions = [SELECT Title, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: contentDocumentIds];

        for(ContentVersion cv : contentVersions) {
            ContentDistribution cd = new ContentDistribution();

            cd.Name = cv.Title;
            cd.ContentVersionId = cv.id;
            cd.PreferencesAllowViewInBrowser= true;
            cd.PreferencesLinkLatestVersion=true;
            cd.PreferencesNotifyOnVisit=false;
            cd.PreferencesPasswordRequired=false;
            cd.PreferencesAllowOriginalDownload= true;

            contentDistributions.add(cd);
        }

        //TODO: add error handling
        insert contentDistributions;

        for(ContentDistribution cd : [SELECT DistributionPublicUrl, ContentDownloadUrl, ContentDocumentId, ContentVersionId FROM ContentDistribution WHERE Id IN: contentDistributions]) {
            for(ContentDocumentLink cdl : contentDocumentLinks) {
                if(cd.ContentDocumentId == cdl.ContentDocumentId) {
                    linkedEntitydToContentDistributionMap.put(cdl.LinkedEntityId, cd);
                }
            }
        }

        return linkedEntitydToContentDistributionMap;
    }


    /**
     * @author: Mateusz Wolak-Książek
     * @description: delete public links from material records (null LinkUrl__c field) and delete linked contentDistributions records
     * @param materialIds
     */
    public static void deletePublicLinks(Set<Id> materialIds) {
        List<WavySales_SE_Material__c> materialsForPublicLinkDeletions = [SELECT LinkUrl__c, DownloadLinkUrl__c FROM WavySales_SE_Material__c WHERE Id IN: materialIds];

        for(WavySales_SE_Material__c mat : materialsForPublicLinkDeletions) {
            mat.LinkUrl__c = null;
            mat.DownloadLinkUrl__c = null;
        }

        List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: materialIds];
        Set<String> contentDocumentIds = CommonUtility.fetchSet(contentDocumentLinks, 'ContentDocumentId');
        List<ContentVersion> contentVersions = [SELECT Title, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: contentDocumentIds];
        List<ContentDistribution> contentDistributions = [SELECT Id FROM ContentDistribution WHERE ContentVersionId IN: contentVersions];

        update materialsForPublicLinkDeletions;

        if(!contentDistributions.isEmpty()) {
            delete contentDistributions;
        }
    }



}