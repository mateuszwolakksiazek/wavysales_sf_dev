public with sharing class MaterialsController {

    private static final String MODE_DEFAULT = 'default';
    private static final String MODE_ALL = 'all';


    public MaterialsController() {

    }

    @AuraEnabled(cacheable=true)
    public static List<WavySales_SE_Material__c> getList(String phrase, String objectApiName, String recordId, String mode) {
        List<String> recordPhrases = new List<String>();
        String tempPhrase;

        if(mode == MODE_DEFAULT) {
            recordPhrases.addAll(getRecordPhrases(objectApiName, recordId));

            if(phrase.length() > 0) {
                //when in default mode, hold search phrase in temporary string, so all materials won't be searched
                tempPhrase = phrase;
                phrase = '';
            }

            if(recordPhrases.isEmpty()) {
                return new List<WavySales_SE_Material__c>();
            }
        }

        if (phrase.length() > 0 || recordPhrases.size() > 0) {
            List<WavySales_SE_Material__c> filteredList = new List<WavySales_SE_Material__c>();

            List<String> phrases = new List<String>();
            if(phrase.length() > 0)         phrases.add(phrase);
            if(recordPhrases.size() > 0)    phrases.addAll(recordPhrases);

            List<WavySales_SE_Material__c> filteredByNameList = MaterialsController.getListFilteredByName(phrases);
            if (filteredByNameList.size() > 0) {
                filteredList.addAll(filteredByNameList);
            }

            List<WavySales_SE_Material__c> filteredByKeywordsList = MaterialsController.getListFilteredByKeywords(phrases);
            if (filteredByKeywordsList.size() > 0) {
                filteredList.addAll(filteredByKeywordsList);
            }

            filterMaterialsOnDefaultModeWithPhrase(filteredList, mode, tempPhrase);

            return filteredList;
        }

        return [SELECT (SELECT Id FROM CombinedAttachments), Id, Name__c, LinkUrl__c, FileMimeType__c, FileSize__c, FileExtension__c, Category__c, Description__c, Application__c, Rating__c FROM WavySales_SE_Material__c];
    }

    @AuraEnabled
    public static Double updateRating(String objectId, Integer rating) {
        WavySales_SE_Material__c material = [SELECT Rating__c, RatingSums__c, RatingQuantity__c FROM WavySales_SE_Material__c WHERE Id = :objectId];
        if (material.RatingSums__c == null) {
            material.RatingSums__c = 0;
        }
        if (material.RatingQuantity__c == null) {
            material.RatingQuantity__c = 0;
        }

        material.RatingSums__c = material.RatingSums__c + rating;
        material.RatingQuantity__c += 1;
        material.Rating__c = material.RatingSums__c / material.RatingQuantity__c;

        update material;

        return material.Rating__c;
    }

    private static List<WavySales_SE_Material__c> getListFilteredByName(List<String> phrases) {
        for(Integer i = 0; i < phrases.size(); i++) {
            phrases[i] = '%' + phrases[i] + '%';
        }

        return [SELECT (SELECT Id FROM CombinedAttachments), (SELECT KeyMessageId__c, KeyMessageId__r.Name__c, KeyMessageId__r.Usage__c, KeyMessageId__r.RecordTypeId FROM WavySales_Key_Message_Relations__r), Id, Name__c, LinkUrl__c, FileMimeType__c, FileSize__c, FileExtension__c, Category__c, Description__c, Application__c, Rating__c FROM WavySales_SE_Material__c WHERE Name__c LIKE: phrases];
    }

    private static List<WavySales_SE_Material__c> getListFilteredByKeywords(List<String> phrases) {
        for(Integer i = 0; i < phrases.size(); i++) {
            phrases[i] = '%' + phrases[i] + '%';
        }
        List<WavySales_KeyMessageRelation__c> keyMessageRelations = [SELECT MaterialId__c FROM WavySales_KeyMessageRelation__c WHERE KeyMessageId__r.Name__c LIKE :phrases AND KeyMessageId__r.Usage__c = :CommonUtility.KEY_MESSAGE_USAGE_SE_MATERIAL AND KeyMessageId__r.RecordTypeId = :CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE, CommonUtility.KEYMESSAGE_TYPE_MATERIAL_KEYWORD)];
        Set<String> materialIds = CommonUtility.fetchSet(keyMessageRelations, 'MaterialId__c');

        return [SELECT (SELECT Id FROM CombinedAttachments), (SELECT KeyMessageId__c, KeyMessageId__r.Name__c, KeyMessageId__r.Usage__c, KeyMessageId__r.RecordTypeId FROM WavySales_Key_Message_Relations__r), Id, Name__c, LinkUrl__c, FileMimeType__c, FileSize__c, FileExtension__c, Category__c, Description__c, Application__c, Rating__c FROM WavySales_SE_Material__c WHERE Id IN: materialIds];
    }

    private static List<String> getRecordPhrases(String objectApiName, String recordId) {
        List<String> fieldValues = new List<String>();

        List<WavySales_SettingValue__c> settingValues = [SELECT Value__c FROM WavySales_SettingValue__c WHERE Name =: objectApiName AND Setting__c IN (SELECT Id FROM WavySales_Setting__c WHERE App__c =: CommonUtility.WAVYSALES_SETTING_APP_SE AND Name =: CommonUtility.DOCS_MATCHING_SETTING_NAME)];

        if(settingValues.size() > 0 && settingValues[0].Value__c != NULL) {

            WavySales_SettingValue__c settingValue = settingValues[0];
            List<String> fieldApiNameList = settingValue.Value__c.split(';');

            String query = new UTIL_Query()
                    .withSelectFields(fieldApiNameList)
                    .withFrom(String.valueOf(objectApiName))
                    .withWhere('Id = \''+ recordId + '\'')
                    .build();

            sObject record = getRecordByQuery(query);

            for(String fieldApiName : fieldApiNameList) {
                fieldValues.add(String.valueOf(record.get(fieldApiName)));
            }

        }

        return fieldValues;
    }

    private static sObject getRecordByQuery(String query) {
        List<sObject> records = Database.query(query);
        if (!records.isEmpty()) {
            return records[0];
        }
        return null;
    }

    public static void filterMaterialsOnDefaultModeWithPhrase(List<WavySales_SE_Material__c> filteredList, String mode, String tempPhrase) {
        if(mode == MODE_DEFAULT && String.isNotBlank(tempPhrase)) {
            for(Integer i = filteredList.size() - 1; i >= 0; i--) {
                if(filteredList[i].Name__c.toLowerCase().containsIgnoreCase(tempPhrase)) {
                    continue;
                } else if(!filteredList[i].WavySales_Key_Message_Relations__r.isEmpty()) {
                    Boolean contains = false;
                    for(WavySales_KeyMessageRelation__c keyMessage : filteredList[i].WavySales_Key_Message_Relations__r) {
                        if(keyMessage.KeyMessageId__c != null && keyMessage.KeyMessageId__r.Usage__c == CommonUtility.KEY_MESSAGE_USAGE_SE_MATERIAL && keyMessage.KeyMessageId__r.RecordTypeId == CommonUtility.getRecordTypeId(CommonUtility.SOBJECT_NAME_WAVY_SALES_KEYMESSAGE, CommonUtility.KEYMESSAGE_TYPE_MATERIAL_KEYWORD)) {
                            if(String.isNotBlank(keyMessage.KeyMessageId__r.Name__c) && keyMessage.KeyMessageId__r.Name__c.toLowerCase().containsIgnoreCase(tempPhrase)) {
                                contains = true;
                                break;
                            }
                        }
                    }
                    if(!contains) {
                        filteredList.remove(i);
                    }
                } else {
                    filteredList.remove(i);
                }
            }
        }
    }
}