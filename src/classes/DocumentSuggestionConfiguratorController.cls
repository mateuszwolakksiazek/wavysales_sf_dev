/**
 * Created by mateusz.wolak on 19.12.2019.
 */

public with sharing class DocumentSuggestionConfiguratorController {


    /**
     * @author: Mateusz Wolak-Książek
     * @description: get schema objects & related fields info
     * @return SchemaObjectListWrapper
     */
    @AuraEnabled(cacheable=true)
    public static List<SchemaObjectWrapper> getSchemaObjectsSrv() {
        List<SchemaObjectWrapper> schemaObjectWrappers = new List<SchemaObjectWrapper>();

        for(String schemaObjectName : SchemaUtils.globalDescribe.keySet()) {
            Schema.SObjectType schemaObjectType = SchemaUtils.globalDescribe.get(schemaObjectName);
            Schema.DescribeSObjectResult describeSObjectResultObj = schemaObjectType.getDescribe();

            if(!describeSObjectResultObj.isCustomSetting()) {
                schemaObjectWrappers.add(new SchemaObjectWrapper(schemaObjectName));
            }

        }

        schemaObjectWrappers.sort();

        return schemaObjectWrappers;
    }

    /**
     * @author: Mateusz Wolak-Książek
     * @description: get schema object fields info
     * @param schemaObjectName
     *
     * @return list of schema field wrappers
     */
    @AuraEnabled(cacheable=true)
    public static List<SchemaFieldWrapper> getSchemaObjectFieldsSrv(String schemaObjectName) {

        List<SchemaFieldWrapper> schemaFieldWrappers = new List<SchemaFieldWrapper>();
        Schema.SObjectType sObjectType = SchemaUtils.globalDescribe.get(schemaObjectName);

        for(Schema.SObjectField field : sObjectType.getDescribe().fields.getMap().values()) {
            Schema.DescribeFieldResult fieldResult = field.getDescribe();
            schemaFieldWrappers.add(new SchemaFieldWrapper(fieldResult.getLabel(), fieldResult.getName()));
        }

        return schemaFieldWrappers;
    }

    /**
     * @author: Mateusz Wolak-Książek
     * @description: method used to save configurations
     *
     * @param objectToFieldsMap
     */
    @AuraEnabled(cacheable=false)
    public static void saveDocumentSuggestionConfigurationSrv(Map<String, List<String>> objectToFieldsMap) {
        if(objectToFieldsMap != null && !objectToFieldsMap.isEmpty()) {
            Map<String, WavySales_SettingValue__c> objectNameToWavySalesSettingMap = getDocumentSuggestionCurrentSettingsSrv();
            List<WavySales_Setting__c> docsMatchingSettings = [SELECT Id FROM WavySales_Setting__c WHERE App__c =: CommonUtility.WAVYSALES_SETTING_APP_SE AND Name =: CommonUtility.DOCS_MATCHING_SETTING_NAME];
            WavySales_Setting__c docsMatchingSetting = docsMatchingSettings.isEmpty() ? new WavySales_Setting__c() : docsMatchingSettings[0];

            for(String objectName : objectToFieldsMap.keySet()) {
                String fieldsJoined = '';

                //join fields with ; delimiter
                if(objectToFieldsMap.get(objectName) != null && !objectToFieldsMap.get(objectName).isEmpty()) {
                    objectToFieldsMap.get(objectName).sort();
                    fieldsJoined = String.join(objectToFieldsMap.get(objectName), ';');
                }

                //change current record if exists or create a new one
                if(objectNameToWavySalesSettingMap.containsKey(objectName)) {
                    WavySales_SettingValue__c settingValue = objectNameToWavySalesSettingMap.get(objectName);
                    settingValue.Value__c = fieldsJoined;
                    objectNameToWavySalesSettingMap.put(objectName, settingValue);
                } else {
                    WavySales_SettingValue__c settingValue = new WavySales_SettingValue__c(Name = objectName, Value__c = fieldsJoined, Setting__c = docsMatchingSetting.Id);
                    objectNameToWavySalesSettingMap.put(objectName, settingValue);
                }
            }

            if(!objectNameToWavySalesSettingMap.isEmpty()) {
                List<WavySales_SettingValue__c> insertList  = new List<WavySales_SettingValue__c>(), updateList = new List<WavySales_SettingValue__c>();

                for(WavySales_SettingValue__c settingValue : objectNameToWavySalesSettingMap.values()) {
                    if(settingValue.Id == null) {
                        insertList.add(settingValue);
                    } else {
                        updateList.add(settingValue);
                    }
                }

                //try {
                    insert insertList;
               // } catch (Exception e) {
                    //handle exception
                //}

               // try {
                    update updateList;
               // } catch (Exception e) {
                    //handle exception
               // }
            }
        }

    }

    /**
     * @author: Mateusz Wolak-Książek
     * @description: get current document suggestion settings
     *
     * @return Map, where key is object name (also setting Name field value), and value is wavysales settingvalue record
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, WavySales_SettingValue__c> getDocumentSuggestionCurrentSettingsSrv() {
        Map<String, WavySales_SettingValue__c> objectNameToWavySalesSettingMap = new Map<String, WavySales_SettingValue__c>();

        List<WavySales_Setting__c> docsMatchingSettings = [SELECT Id FROM WavySales_Setting__c WHERE App__c =: CommonUtility.WAVYSALES_SETTING_APP_SE AND Name =: CommonUtility.DOCS_MATCHING_SETTING_NAME];

        if(!docsMatchingSettings.isEmpty()) {
            for(WavySales_SettingValue__c settingValue : [SELECT Name, Value__c, Setting__c FROM WavySales_SettingValue__c WHERE Setting__c IN: docsMatchingSettings]) {
                objectNameToWavySalesSettingMap.put(settingValue.Name, settingValue);
            }
        } else { //create new setting if not present
            //try {
                insert new WavySales_Setting__c(Name = CommonUtility.DOCS_MATCHING_SETTING_NAME, App__c = CommonUtility.WAVYSALES_SETTING_APP_SE, ComponentSource__c = CommonUtility.WAVYSALES_SETTING_COMPONENTSOURCE_SF, ComponentType__c = CommonUtility.WAVYSALES_SETTING_COMPONENTTYPE_RECORDSLISTPAGE);
            //} catch (Exception e) {
                //handle exception
            //}
        }

        return objectNameToWavySalesSettingMap;
    }


    public class SchemaObjectWrapper implements Comparable {
        @AuraEnabled public String label;
        @AuraEnabled public Boolean selected;

        public SchemaObjectWrapper(String label) {
            this.label = label;
            this.selected = false;
        }

        public Integer compareTo(Object compareTo) {
            SchemaObjectWrapper schemaToCompare = (SchemaObjectWrapper)compareTo;

            Integer returnValue = 0;
            if(this.label == schemaToCompare.label) {
                returnValue = 0;
            } else if (this.label < schemaToCompare.label) {
                returnValue = -1;
            } else {
                returnValue = 1;
            }

            return returnValue;
        }
    }

    public class SchemaFieldWrapper {
        @AuraEnabled public String label;
        @AuraEnabled public String value; //apiName
        @AuraEnabled public Boolean selected;

        public SchemaFieldWrapper(String label, String value) {
            this.label = label;
            this.value = value;
            this.selected = false;
        }
    }

}