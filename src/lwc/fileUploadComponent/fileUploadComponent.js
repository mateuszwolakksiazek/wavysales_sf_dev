/**
 * Created by mateusz.wolak on 13.10.2019.
 */

import { LightningElement, api, track, wire } from 'lwc';
// imported to show toast messages
import {ShowToastEvent} from 'lightning/platformShowToastEvent';


/* form section */

// import uiRecordApi to create record
import { createRecord, getRecord, getFieldValue  } from 'lightning/uiRecordApi';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
// importing Material fields
import MATERIAL_OBJECT from '@salesforce/schema/WavySales_SE_Material__c';
import MATERIAL_NAME_FIELD from '@salesforce/schema/WavySales_SE_Material__c.Name__c';
import MATERIAL_APPLICATION_FIELD from '@salesforce/schema/WavySales_SE_Material__c.Application__c';
import MATERIAL_AUTHOR_FIELD from '@salesforce/schema/WavySales_SE_Material__c.AuthorId__c';
import MATERIAL_CATEGORY_FIELD from '@salesforce/schema/WavySales_SE_Material__c.Category__c';
import MATERIAL_DESCRIPTION_FIELD from '@salesforce/schema/WavySales_SE_Material__c.Description__c';
//import Key Message
import KEYMESSAGE_OBJECT from '@salesforce/schema/WavySales_KeyMessage__c';

// rest of not displayed fields
import MATERIAL_FILEXTENSION_FIELD from '@salesforce/schema/WavySales_SE_Material__c.FileExtension__c';
import MATERIAL_FILEMIMETYPE_FIELD from '@salesforce/schema/WavySales_SE_Material__c.FileMimeType__c';
//import labels
import next from '@salesforce/label/c.Next';
import save from '@salesforce/label/c.Save';
import cancel from '@salesforce/label/c.Cancel';
import fillMaterialInfo from '@salesforce/label/c.Fill_Material_Info';
import searchKeyMessages from '@salesforce/label/c.Search_Key_Messages';

import getContentDocumentsInfo from '@salesforce/apex/FileUploadController.getContentDocumentsInfo'
import createMaterialRecords from '@salesforce/apex/FileUploadController.createMaterialRecords'
import deleteContentDocuments from '@salesforce/apex/FileUploadController.deleteContentDocuments'


// importing ContentDocument fields
//TODO: remove (?)
import CONTENTDOCUMENT_OBJECT from '@salesforce/schema/ContentDocument';
import CONTENTDOCUMENT_CONTENTSIZE_FIELD from '@salesforce/schema/ContentDocument.ContentSize';
import CONTENTDOCUMENT_FILEEXTENSION_FIELD from '@salesforce/schema/ContentDocument.FileExtension';
import CONTENTDOCUMENT_FILETYPE_FIELD from '@salesforce/schema/ContentDocument.FileType';
import CONTENTDOCUMENT_TITLE_FIELD from '@salesforce/schema/ContentDocument.Title';
import CONTENTDOCUMENT_OWNERID_FIELD from '@salesforce/schema/ContentDocument.OwnerId';



export default class FileUploadComponent extends LightningElement {
    @api
    recordId;

    @track
    openModal = false;

    @api objectApiName;

    currentFileIndex = 0;

    @track contentDocuments;
    @track error;

    @track contDocIds;

    @track lightningCardMaterialInfo;
    @track modalSaveButtonLabel;

    label = {
        next,
        save,
        cancel,
        fillMaterialInfo,
        searchKeyMessages
     };

    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        this.contDocIds = uploadedFiles.map(con => con.documentId);

        getContentDocumentsInfo({ documentIds : this.contDocIds}).then(result => {
            this.contentDocuments = result;

            if(this.contentDocuments.length > this.currentFileIndex) {

                this.materialRecord = {
                    Name : typeof this.contentDocuments[this.currentFileIndex].Title !== 'undefined' ? this.contentDocuments[this.currentFileIndex].Title : MATERIAL_NAME_FIELD,
                    Application : '',
                    AuthorName : typeof this.contentDocuments[this.currentFileIndex].Owner.Name !== 'undefined' ? this.contentDocuments[this.currentFileIndex].Owner.Name : '',
                    AuthorId : typeof this.contentDocuments[this.currentFileIndex].OwnerId !== 'undefined' ? this.contentDocuments[this.currentFileIndex].OwnerId : '',
                    Category : '',
                    Description : '',
                    ContentDocumentId : typeof this.contentDocuments[this.currentFileIndex].Id !== 'undefined' ? this.contentDocuments[this.currentFileIndex].Id : '',
                    ContentSize : typeof this.contentDocuments[this.currentFileIndex].ContentSize !== 'undefined' ? this.contentDocuments[this.currentFileIndex].ContentSize : 0,
                    FileExtension : typeof this.contentDocuments[this.currentFileIndex].FileExtension !== 'undefined' ? this.contentDocuments[this.currentFileIndex].FileExtension : '',
                    FileType : typeof this.contentDocuments[this.currentFileIndex].FileType !== 'undefined' ? this.contentDocuments[this.currentFileIndex].FileType : '',
                    PublicLink : false,
                    KeyMessages : [],
                    NewKeyMessages : []
                };
                this.lightningCardMaterialInfo = this.label.fillMaterialInfo;
                this.modalSaveButtonLabel = (this.currentFileIndex + 1 === this.contentDocuments.length ) ? 'Save' : 'Next';
                this.openModal = true;
            }

          }).catch(error => {
            this.error = error;
            console.log('error');
        })

    }

    closeModal() {
        this.openModal = false;
    }

    /* form section */

    @track error;

    @wire(getObjectInfo, { objectApiName: MATERIAL_OBJECT })
    materialInfo;

    @wire(getPicklistValues, { recordTypeId: '$materialInfo.data.defaultRecordTypeId', fieldApiName: MATERIAL_APPLICATION_FIELD})
    ApplicationPicklistValues;

    @wire(getPicklistValues, { recordTypeId: '$materialInfo.data.defaultRecordTypeId', fieldApiName: MATERIAL_CATEGORY_FIELD})
    CategoryPicklistValues;

    @wire(getObjectInfo, { objectApiName: KEYMESSAGE_OBJECT })
    keyMessageInfo;

    materialRecords = [];

    get materialFields() {
        return this.materialInfo.data.fields;
    }

    get keyMessageLabel() {
        return this.keyMessageInfo.data.labelPlural;
    }

    // this object have record information
    @track
    materialRecord = undefined /*{
        Name : MATERIAL_NAME_FIELD,
        Application : MATERIAL_APPLICATION_FIELD,
        Author : MATERIAL_AUTHOR_FIELD,
        Category : MATERIAL_CATEGORY_FIELD,
        Description : MATERIAL_DESCRIPTION_FIELD
    };*/

    @track
     _applicationFieldSelected = [];

     @track
      _categoryFieldSelected = [];

    handleNameChange(event) {
        this.materialRecord.Name = event.target.value;
    }

    handleAuthorChange(event) {
         this.materialRecord.Author = event.detail.recordId;
         this.materialRecord.AuthorId = event.detail.recordId;
         this.materialRecord.AuthorName = event.detail.fieldValue;
    }

    handleKeyMessageChange(event) {
        if(event.detail.selRecords.length > 0) {
            const keyMessageIds = [];
            const newKeyMessageNames = [];
            event.detail.selRecords.forEach(function (item, index) {
                if(item.recId) {
                   keyMessageIds.push(item.recId);
                } else {
                    newKeyMessageNames.push(item.recName);
                }
           });
           this.materialRecord.KeyMessages = keyMessageIds;
           this.materialRecord.NewKeyMessages = newKeyMessageNames;
        } else {
            this.materialRecord.KeyMessages = [];
            this.materialRecord.NewKeyMessages = [];
        }
    }

    handleDescriptionChange(event) {
        this.materialRecord.Description = event.target.value;
    }

    handleApplicationChange(e) {
        this._applicationFieldSelected = e.detail.value;
    }

    handleCategoryChange(e) {
        this._categoryFieldSelected = e.detail.value;
    }

    handlePublicLinkChange(event) {
        this.materialRecord.PublicLink = event.target.checked;
    }

    handleMaterialRecordChanges() {

    }

    /*********************************************************************************
    * cancel method - deletes uploaded files, fires a toast and invokes reload method
    **/
    handleCancel() {

        deleteContentDocuments({ documentIds : this.contDocIds}).then(result => {

               console.log('records deletion result' + result)
               this.dispatchEvent(
                   new ShowToastEvent({
                       title: 'Success',
                       message: 'Material records successfully deleted',
                       variant: 'error',
                   }),
               );

               setTimeout(() => {
                   location.reload();
               }, 1500);

        }).catch(error => {
            this.error = error;
            console.log('error');
        })

        this.closeModal();

    }

    handleSave() {
        try{

        //set material records field values using array values
        this.materialRecord.Category = (typeof this._categoryFieldSelected !== 'undefined' && this._categoryFieldSelected.length > 0) ? Array.prototype.join.call(this._categoryFieldSelected, ';') : '';
        this.materialRecord.Application = (typeof this._applicationFieldSelected !== 'undefined' && this._applicationFieldSelected.length > 0) ? Array.prototype.join.call(this._applicationFieldSelected, ';') : '';
        this.materialRecord.Author = this.materialRecord.AuthorId;
        this.materialRecord.WhatId = this.recordId;
        // clear arrays for new inputs
        this._categoryFieldSelected = [];
        this._applicationFieldSelected = [];
        //add material to array
        this.materialRecords.push(this.materialRecord);
        //clear values and set new predefined values
        let categoryData = this.CategoryPicklistValues.data;
        let applicationData = this.ApplicationPicklistValues.data;
        this.CategoryPicklistValues.data = false;
        this.ApplicationPicklistValues.data = false;
        //workound to clear lightning-dual-listbox inputs (lwc bug)
        setTimeout(() => {
            this.CategoryPicklistValues.data = categoryData;
            this.ApplicationPicklistValues.data = applicationData;
            this.materialRecord = {
                Name : typeof this.contentDocuments[this.currentFileIndex].Title !== 'undefined' ? this.contentDocuments[this.currentFileIndex].Title : MATERIAL_NAME_FIELD,
                Application : '' /*MATERIAL_APPLICATION_FIELD*/,
                AuthorName : typeof this.contentDocuments[this.currentFileIndex].Owner.Name !== 'undefined' ? this.contentDocuments[this.currentFileIndex].Owner.Name : '',
                AuthorId : typeof this.contentDocuments[this.currentFileIndex].OwnerId !== 'undefined' ? this.contentDocuments[this.currentFileIndex].OwnerId : '',
                Category : '' /*MATERIAL_CATEGORY_FIELD*/,
                Description : '' /*MATERIAL_DESCRIPTION_FIELD*/,
                ContentDocumentId : typeof this.contentDocuments[this.currentFileIndex].Id !== 'undefined' ? this.contentDocuments[this.currentFileIndex].Id : '',
                ContentSize : typeof this.contentDocuments[this.currentFileIndex].ContentSize !== 'undefined' ? this.contentDocuments[this.currentFileIndex].ContentSize : 0,
                FileExtension : typeof this.contentDocuments[this.currentFileIndex].FileExtension !== 'undefined' ? this.contentDocuments[this.currentFileIndex].FileExtension : '',
                FileType : typeof this.contentDocuments[this.currentFileIndex].FileType !== 'undefined' ? this.contentDocuments[this.currentFileIndex].FileType : '',
                PublicLink : false,
                KeyMessages : [],
                NewKeyMessages : []
            }
        },0);

        if( this.currentFileIndex + 1 >= this.contentDocuments.length ) {

            const materialRecordsToCreate = [];
            this.materialRecords.forEach(function (item, index) {
                const materialObj = {};

                materialObj[MATERIAL_NAME_FIELD.fieldApiName] = item.Name;
                materialObj[MATERIAL_APPLICATION_FIELD.fieldApiName] = item.Application;
                materialObj[MATERIAL_AUTHOR_FIELD.fieldApiName] = item.Author;
                materialObj[MATERIAL_CATEGORY_FIELD.fieldApiName] = item.Category;
                materialObj[MATERIAL_DESCRIPTION_FIELD.fieldApiName] = item.Description;

                materialRecordsToCreate.push(materialObj);

            });

           createMaterialRecords({ materialRecordsJSON : JSON.stringify(this.materialRecords) }).then(result => {

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Material records successfully created!',
                        variant: 'success',
                    }),
                );

                setTimeout(() => {
                    location.reload();
                }, 1000);

              }).catch(error => {
                this.error = error;
                console.error(error);
            })


            this.materialRecord = undefined;
            this.closeModal();
        }

        //increment current file index
        this.currentFileIndex = this.currentFileIndex + 1;
        //set lightning card and change button label if needed
        this.lightningCardMaterialInfo = this.label.fillMaterialInfo;
        this.modalSaveButtonLabel = (this.currentFileIndex + 1 === this.contentDocuments.length ) ? this.label.save : this.label.next;


        }catch(error) {
            console.error(error);
        }

    }


}