/**
 * Created by mateusz.wolak on 01.12.2019.
 */

import { LightningElement,api,track } from 'lwc';
import getResults from '@salesforce/apex/LwcMultiLookupController.getResults';
import getRelatedRecordResults from '@salesforce/apex/LwcMultiLookupController.getRelatedRecordResults';
import createKeyMessageWithRelation from '@salesforce/apex/LwcMultiLookupController.createKeyMessageWithRelation';
import createKeyMessageRelation from '@salesforce/apex/LwcMultiLookupController.createKeyMessageRelation';
import deleteKeyMessageRelation from '@salesforce/apex/LwcMultiLookupController.deleteKeyMessageRelation';


//labels
import noResultsFound from '@salesforce/label/c.No_result_found';
import loading from '@salesforce/label/c.Loading';

export default class LwcMultiLookup extends LightningElement {
    @api recordId;
    @api objectName = 'Account';
    @api fieldName = 'Name';
    @api additionalWhereConditions;
    @api labelVal;
    @api placeholderLabel;
    @track searchText = '';
    @track searchRecords = [];
    @track selectedRecords = [];
    @api required = false;
    @api iconName = 'action:new_account'
    @api LoadingText = false;
    @api hideEditMode = false;
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track noResultsFoundFlag = false;
    @track newLabelFlag = false;
    @track showCloseIcon = false;
    //related list section
    @api displayAsRelated = false;
    @api searchRelatedObjectName;
    @api searchRelatedFieldName;
    @api searchRelatedWhereName;

    relatedInitialized = false;

    label = {
        noResultsFound,
        loading
     };

    renderedCallback() {

        if(this.relatedInitialized) {
            return;
        }

        if(this.displayAsRelated) {
            getRelatedRecordResults({ searchRelatedObjectName: this.searchRelatedObjectName, searchRelatedFieldName: this.searchRelatedFieldName, searchRelatedWhereName : this.searchRelatedWhereName, recordId: this.recordId })
            .then(result => {
                if(result && result.length > 0) {
                    this.selectedRecords = result;
                }
            })
            .catch(error => {
                console.log('-------error-------------'+ JSON.stringify(error));
                console.log(error);
            });
        }

        this.relatedInitialized = true;
    }

    searchField(event) {

        this.searchText = event.target.value;
        var selectRecId = [];
        for(let i = 0; i < this.selectedRecords.length; i++){
            selectRecId.push(this.selectedRecords[i].recId);
        }
        this.LoadingText = true;
        this.noResultsFoundFlag = false;
        this.showCloseIcon = true;
        getResults({ ObjectName: this.objectName, fieldName: this.fieldName, value: this.searchText, selectedRecId : selectRecId, additionalWhereClause : this.additionalWhereConditions })
        .then(result => {
            this.searchRecords = result;
            this.LoadingText = false;

            this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            if(this.searchText.length > 0 && result.length == 0) {
                var containsNewLabel = false;
                this.selectedRecords.forEach(item => {
                    if(item.recName === this.searchText)
                         containsNewLabel = true;
                });
                if(!containsNewLabel) {
                    this.noResultsFoundFlag = true;
                    this.newLabelFlag = true;
                } else {
                    this.newLabelFlag = false;
                }
            }
            else {
                this.noResultsFoundFlag = false;

                //check if add button should be display only when user enterted text
                if(this.searchText.length > 0) {
                    var containsNewLabel = false;
                    this.searchRecords.forEach(item => {
                        if(item.recName.toLowerCase() === this.searchText.toLowerCase()) {
                             containsNewLabel = true;
                        }
                    });
                    if(!containsNewLabel) {
                        this.newLabelFlag = true;
                    } else {
                        this.newLabelFlag = false;
                    }
                } else {
                    this.newLabelFlag = false;
                }


                if(this.displayAsRelated) {
                    let searchRecordsTemp = [...this.searchRecords];

                    for(var i = searchRecordsTemp.length - 1; i >= 0; i--){
                        for( var j = 0; j < this.selectedRecords.length; j++){
                            if(searchRecordsTemp[i] && (searchRecordsTemp[i].recName === this.selectedRecords[j].recName)){
                                searchRecordsTemp.splice(i, 1);
                            }
                        }
                    }
                    this.searchRecords = searchRecordsTemp;
                }
            }

            if(this.selectRecordId != null && this.selectRecordId.length > 0) {
                this.iconFlag = false;
                this.clearIconFlag = true;
            }
            else {
                this.iconFlag = true;
                this.clearIconFlag = false;
            }
        })
        .catch(error => {
            console.log('-------error-------------'+ JSON.stringify(error));
            console.log(error);
        });

    }

    onAddClick(event) {
        var selectName = this.searchText;
        let newsObject = { 'recId' : undefined ,'recName' : selectName, 'recNameWithId' : selectName };
        this.selectedRecords.push(newsObject);
        let selRecords = this.selectedRecords;
        this.template.querySelectorAll('lightning-input').forEach(each => {
            each.value = '';
        });
        const selectedEvent = new CustomEvent('selected', { detail: {selRecords}, });
        this.showCloseIcon = false;
        this.noResultsFoundFlag = false;
        this.newLabelFlag = false;
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);

        if(this.displayAsRelated) {

             createKeyMessageWithRelation({ recordId: this.recordId, keyMessageName: selectName })
            .then(result => {

            })
            .catch(error => {
                console.log('-------error-------------'+ JSON.stringify(error));
                console.log(error);
            });
        }

    }

    onCloseClick(event) {
        this.showCloseIcon = false;
        this.noResultsFoundFlag = false;
        this.newLabelFlag = false;
        this.searchText = '';
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    }

   setSelectedRecord(event) {
        var recId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
        let newsObject = { 'recId' : recId ,'recName' : selectName, 'recNameWithId' : selectName + '-' + recId };
        this.selectedRecords.push(newsObject);
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        let selRecords = this.selectedRecords;
		this.template.querySelectorAll('lightning-input').forEach(each => {
            each.value = '';
        });
        const selectedEvent = new CustomEvent('selected', { detail: {selRecords}, });
        this.showCloseIcon = false;
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);

        if(this.displayAsRelated) {

            createKeyMessageRelation({ recordId: this.recordId, keyMessageId: recId })
            .then(result => {

            })
            .catch(error => {
                console.log('-------error-------------'+ JSON.stringify(error));
                console.log(error);
            });

        }
    }

    removeRecord (event){
        if(!this.hideEditMode) {
            let selectRecId = [];
            for(let i = 0; i < this.selectedRecords.length; i++){
                if(event.detail.name !== this.selectedRecords[i].recNameWithId)
                    selectRecId.push(this.selectedRecords[i]);
            }
            this.selectedRecords = [...selectRecId];
            let selRecords = this.selectedRecords;
            const selectedEvent = new CustomEvent('selected', { detail: {selRecords}, });
            // Dispatches the event.
            this.dispatchEvent(selectedEvent);

            if(this.displayAsRelated) {
                let recInfo = event.detail.name.split('-');
                let recId = recInfo.length > 1 ? recInfo[1] : undefined;
                if(recId) {
                     deleteKeyMessageRelation({ keyMessageRelationId: recId })
                    .then(result => {

                    })
                    .catch(error => {
                        console.log('-------error-------------'+ JSON.stringify(error));
                        console.log(error);
                    });
                }
            }
        }
    }
}