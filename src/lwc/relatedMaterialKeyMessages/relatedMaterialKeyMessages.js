/**
 * Created by mateusz.wolak on 09.12.2019.
 */

import { LightningElement, api, track } from 'lwc';

export default class RelatedMaterialKeyMessages extends LightningElement {

    @api recordId;
    @track hideEditMode = true;


    onEditClick(event) {
        this.hideEditMode = !this.hideEditMode;
    }

}