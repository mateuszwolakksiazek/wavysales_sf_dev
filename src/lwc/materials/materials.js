import { LightningElement, wire, track, api } from 'lwc';
import getList from '@salesforce/apex/MaterialsController.getList';

export default class Materials extends LightningElement {
    @track phrase = '';

    @track list;
    @track error;

    @api objectApiName;
    @api recordId;

    @wire(getList, { phrase: '$phrase', objectApiName: '$objectApiName', recordId : '$recordId' }) materialsList({ error, data }) {
        if (data) {
            this.list = data.map(function (material) {
                let materialWithLink = {};
                Object.keys(material).forEach(function (key) {
                    materialWithLink[key] = material[key];
                });
                materialWithLink.link = '/sfc/servlet.shepherd/document/download/' + material.CombinedAttachments[0].Id;

                return materialWithLink;
            });
        }
        if (error) {
            this.error = error;
        }
    }

    phraseChange(event) {
        this.phrase = event.target.value;
    }
}