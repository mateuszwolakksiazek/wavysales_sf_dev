trigger PublishProcedure on Procedure__c (before update) {


Map<String, Id> typeMap = New Map<String, Id>();
  for(RecordType rt: [Select DeveloperName, Id From RecordType Where sObjectType = 'Procedure__c' and DeveloperName ='Locked']) {
    typeMap.put(rt.DeveloperName, rt.id);
  }

  for (Procedure__c proc : trigger.new)  {        
      // And the Agreement Category on the record = TEST
      if (proc.Status_w__c == 'Published') {
        // Then automatically change the Record Type to ReadOnly RecordType
        proc.RecordTypeID = typeMap.get('Locked'); 
      }         
  }

}