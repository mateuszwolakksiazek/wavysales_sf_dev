trigger ProcedureLockBeforeDelete on Procedure__c (after delete) {
	CancelProcedure.updateDeleted(Trigger.oldMap);
}