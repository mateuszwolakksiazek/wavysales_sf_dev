({
	init: function (cmp, event, helper) {
        cmp.set('v.columns1', [
            {label: 'Cecha', fieldName: 'question', type: 'text'},
            {label: 'Ocena', fieldName: 'result', type: 'percent', cellAttributes: { iconName: { fieldName: 'trendIcon' }, iconPosition: 'right' }},
        ]);
        
        cmp.set('v.data1', [
            { id: 'a', question: 'Pewność siebie', result: 0.2, },
            { id: 'b', question: 'Artykulacja', result: 0.4, },
            { id: 'c', question: 'Przekaz', result: 0.54, },
            { id: 'd', question: 'Sposób prezentacji', result: 0.1, },
            { id: 'e', question: 'Język korzyści', result: 1, }
        ]);
        
       
        
        
    }
})