/**
 * Created by mateusz.wolak on 19.12.2019.
 */

({
    /**
    * @description: invoke init setup methods
    */
    doInit: function(component, event, helper) {
        component.set("v.isLoading", true);

         const promises = [
            this.getSchemaObjects(component, event, helper),
            this.getDocumentSuggestionCurrentSettings(component, event, helper)
        ];

        return Promise.all(promises);
    },

    /**
    * @description: get schema data from APEX and set as attribute
    */
    getSchemaObjects: function(component, event, helper) {
         return new Promise( (resolve, reject) => {

            var action = component.get("c.getSchemaObjectsSrv");

            if(typeof action === 'undefined') {
                resolve();
            } else {
                action.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var data = response.getReturnValue();
                    //if record is correct
                    if(data != null) {
                        component.set("v.schemaObjects", data);
                    } else {
                        reject();
                    }

                }
                else if (state === "INCOMPLETE") {
                }
                else if (state === "ERROR") {
                    // DEBUGGERS for errors
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    reject(errors);
                }

                    component.set("v.isLoading", false);
                    resolve();
                });

                $A.enqueueAction(action);
            }

          })
    },

      /**
     * @description: invoke init setup methods
     */
     doOnObjectChange: function(component, event, helper) {

        component.set("v.isLoading", true);

         var action = component.get("c.getSchemaObjectFieldsSrv");
         var objectName = event.getSource().get("v.value");

         if(objectName === '') {

             component.set("v.fieldValues", []);
             component.set("v.schemaObjectFields", []);
             component.set("v.isLoading", false);

         } else {

             action.setParams({ schemaObjectName : objectName });

             if(typeof action === 'undefined') {
                 resolve();
             } else {
                 action.setCallback(this, function(response) {
                 var state = response.getState();
                 if (component.isValid() && state === "SUCCESS") {
                     var data = response.getReturnValue();
                     //if record is correct
                     if(data != null) {
                         component.set("v.schemaObjectFields", data);

                          let objectToFieldsMap = component.get("v.objectToFieldsMap");
                          let selectedObject = component.get("v.selectedObject");

                          if(objectToFieldsMap[selectedObject]) {
                              component.set("v.fieldValues", objectToFieldsMap[selectedObject]);
                          } else {
                               component.set("v.fieldValues", []);
                          }
                     } else {
                         reject();
                     }

                 }
                 else if (state === "INCOMPLETE") {
                 }
                 else if (state === "ERROR") {
                     // DEBUGGERS for errors
                     var errors = response.getError();
                     if (errors) {
                         if (errors[0] && errors[0].message) {
                             console.log("Error message: " +
                                      errors[0].message);
                         }
                     } else {
                         console.log("Unknown error");
                     }
                     reject(errors);
                 }
                    component.set("v.isLoading", false);
                     resolve();
                 });

                 $A.enqueueAction(action);
             }
         }

     },

     doOnFieldChange: function(component, event, helper) {

         let objectToFieldsMap = component.get("v.objectToFieldsMap");
         let selectedObject = component.get("v.selectedObject");
         let initObjectToFieldsMap = component.get("v.initObjectToFieldsMap");

         objectToFieldsMap[selectedObject] = event.getParam("value");

         component.set("v.objectToFieldsMap", objectToFieldsMap);
         component.set("v.isConfigurationChanged", JSON.stringify(objectToFieldsMap) !== JSON.stringify(initObjectToFieldsMap));

     },

    getDocumentSuggestionCurrentSettings: function(component, event, helper) {

       return new Promise( (resolve, reject) => {

          var action = component.get("c.getDocumentSuggestionCurrentSettingsSrv");

          if(typeof action === 'undefined') {
              resolve();
          } else {
              action.setCallback(this, function(response) {
              var state = response.getState();
              if (component.isValid() && state === "SUCCESS") {
                  var data = response.getReturnValue();

                  //if record is correct
                  if(data) {
                      let objectToFieldsMap = {};

                      var settings = Object.values(data);
                      settings.forEach(function(setting) {
                           objectToFieldsMap[setting.Name] = typeof setting.Value__c !== 'undefined' ? setting.Value__c.split(';') : [];
                      });

                      var clonedObjectToFieldsMap = Object.assign({}, objectToFieldsMap);

                      component.set("v.objectToFieldsMap", objectToFieldsMap);
                      component.set("v.initObjectToFieldsMap", clonedObjectToFieldsMap);

                  } else {
                      reject();
                  }
              }
              else if (state === "INCOMPLETE") {
              }
              else if (state === "ERROR") {
                  // DEBUGGERS for errors
                  var errors = response.getError();
                  if (errors) {
                      if (errors[0] && errors[0].message) {
                          console.log("Error message: " +
                                   errors[0].message);
                      }
                  } else {
                      console.log("Unknown error");
                  }
                  reject(errors);
              }

                  component.set("v.isLoading", false);
                  resolve();
              });

              $A.enqueueAction(action);
          }

       })


    },

    doOnSave: function(component, event, helper) {

        component.set("v.isLoading", true);

        return new Promise( (resolve, reject) => {

            var action = component.get("c.saveDocumentSuggestionConfigurationSrv");
            var objectToFieldsMap = component.get("v.objectToFieldsMap");

            action.setParams({ objectToFieldsMap : objectToFieldsMap });

            if(typeof action === 'undefined') {
                resolve();
            } else {
                action.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {

                    let clonedObjectToFieldsMap = Object.assign({}, objectToFieldsMap);
                    component.set("v.initObjectToFieldsMap", clonedObjectToFieldsMap);
                    component.set("v.isConfigurationChanged", false);

                    component.find('notifLib')
                        .showToast({
                             "variant": "success",
                             "title": $A.get("$Label.c.DocumentSuggestionConfigurator_SavedSuccessful"),
                             "message": $A.get("$Label.c.DocumentSuggestionConfigurator_AllConfigurationSaved")
                         });

                }
                else if (state === "INCOMPLETE") {
                }
                else if (state === "ERROR") {
                    // DEBUGGERS for errors
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                     errors[0].message);

                             component.find('notifLib')
                                .showToast({
                                     "variant": "error",
                                     "title": $A.get("$Label.c.ErrorOccured"),
                                     "message": errors[0].message,
                                     "mode": "sticky"
                                 });
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    reject(errors);
                }

                    component.set("v.isLoading", false);
                    resolve();
                });

                $A.enqueueAction(action);
            }

          })

    },


    doOnClear: function(component, event, helper) {

        component.set("v.isLoading", true);

        let initObjectToFieldsMap = component.get("v.initObjectToFieldsMap");
        let clonedInitObjectToFieldsMap = Object.assign({}, initObjectToFieldsMap);
        let selectedObject = component.get("v.selectedObject");

        component.set("v.objectToFieldsMap", clonedInitObjectToFieldsMap);
        component.set("v.isConfigurationChanged", false);

        if(clonedInitObjectToFieldsMap[selectedObject]) {
          component.set("v.fieldValues", clonedInitObjectToFieldsMap[selectedObject]);
        } else {
           component.set("v.fieldValues", []);
        }

        component.set("v.isLoading", false);

    },

});