/**
 * Created by mateusz.wolak on 19.12.2019.
 */

({
    /**
    * @description: invoke helper init function
    */
    init: function (component, event, helper) {
        helper.doInit(component, event, helper);
    },

    /**
    * @description: invoke helper init function
    */
    onObjectChange: function (component, event, helper) {
        helper.doOnObjectChange(component, event, helper);
    },

    /**
    * @description: invoke helper init function
    */
    onFieldChange: function (component, event, helper) {
         helper.doOnFieldChange(component, event, helper);
    },

    /**
    * @description: invoke helper init function
    */
    onSave: function (component, event, helper) {
         helper.doOnSave(component, event, helper);
     },

    /**
    * @description: invoke helper init function
    */
    onClear: function (component, event, helper) {
         helper.doOnClear(component, event, helper);
    },



});